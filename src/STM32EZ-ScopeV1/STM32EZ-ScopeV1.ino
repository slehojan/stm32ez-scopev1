/* Semestrální práce PPC Jan Šlehofer
 *  
 * STM32EZ-Scope v1.0
 * Program digitálního osciloskopu.
 * Urceno pro procesor STM32F103CB
 */
#define verze "V1.0"
 
//knihovny
#include <EEPROM.h>
#include <Adafruit_ILI9341_STM.h>
#include <Adafruit_GFX_AS.h>
#include <SPI.h>
#include <SdFat.h>
#include <XPT2046_touch.h>

//rychle ADC
#define BKP_REG_BASE   (uint32_t *)(0x40006C00 +0x04)
#define analogInPin PA1
#define TriggerPin PA3
bool trigged = 0;
bool newSamples = 0;
bool slowSampling = 0;
bool slowTstage = 0;
uint16_t timeout = 0;
uint16_t sampleIter = 0;
//pocet vzorku
#define maxSamples 1024*6
#define DEFnullpoint 2040
#define DEFnullpointb 2040
uint16_t nullpoint[2] = {2040, 2040};
#define DEFnullpoint2 2040
#define DEFnullpoint2b 2040
uint16_t nullpoint2[2] = {2040, 2040};
//pole pro data z ADC
int32_t dataPoints32[maxSamples / 2];
int16_t *dataPoints = (int16_t *)&dataPoints32;
//konec DMA indikace
volatile static bool dma1_ch1_Active;
#define ADC_CR1_FASTINT 0x70000 // Fast interleave rezim

//sinc FIR interpol        0         1         2         3        4        5        6        7        8        9        10       11       12       13       14       15
const float FIRcoef[16] = {-0.00693, -0.00828, -0.00469, 0.00721, 0.03087, 0.06928, 0.12429, 0.19607, 0.28272, 0.38014, 0.48227, 0.58162, 0.67008, 0.73988, 0.78460, 0.80000};
//snimani baterie
#define batSens PA0
byte batLvl = 6; //0 min, 6 max
byte curbatLvl = 255;
unsigned int BatLimit = 0;

//SD
SdFat SD(2);
SdFile file;

#define FILE_BASE_NAME "Waveform"
#define SD_CS PA8
bool SDon = 0;

//dotyk
#define Touch_CS PB10
SPIClass SPI_2(2);
XPT2046_touch ts(Touch_CS, SPI_2);
uint16_t xy[2];
unsigned int TouchLimit = 0;
bool touchPressed = 0;
unsigned int touchInterval = 0;
#define touchThr 2000
//kalibrace dotyku
#define TS_MINX 420
#define TS_MINY 200
#define TS_MAXX 3870
#define TS_MAXY 3770

//TFT display
#define TFT_CS PC13 //place holder
#define TFT_DC PB0
#define TFT_RST PB1
Adafruit_ILI9341_STM tft = Adafruit_ILI9341_STM(TFT_CS, TFT_DC, TFT_RST);
byte page = 0;
byte TFTpage = 255;
unsigned int TFTLimit = 0;

//parametry stinitka
#define RetColor ILI9341_WHITE
#define beamColor ILI9341_YELLOW
#define RetWidth 280
#define RetHeight 180
#define RetStartX 21
#define RetStartY 31
#define RetXDiv 10
#define RetYDiv 8

const float sRateTable[3] = {8.1672, 146.96, 956.05};
//8.1064, 145.8666, 952.24
byte tB = 0;        //casova zakladna  0                             ,1                     ,2
byte osciDisplay[139];            //   0  1  2  3   4   5   6    7    8    9     10    11    12     13     14
const uint16_t osciTimeDivTable[15] = {1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000}; //1us - 50000us(50ms), 2 5 10 krok, 15 kroku
byte osciTimeDiv = 3;
byte curosciTimeDiv = 255;
//posuv casove zakladny
uint16_t osciHpos = 0;
bool curosciRun = 0;
bool osciRun = 1;  //1 start, 0 stop
//                   vertikalni kanal 0   1   2   3    4    5    6     7     8
const uint16_t osciVoltDivTable[9] = {10, 20, 50, 100, 200, 500, 1000, 2000, 5000}; //10mV - 5000mV(10V), 2 5 10 krok, 9 kroku
//                                0          1          2          3          4          5          6          7          8
const byte osciVSelectTable[9] = {B01100110, B01100110, B01100110, B01100110, B00100100, B00000000, B11100111, B10100101, B10000001};
//                             2x5, 2x, x10
//sensitivity control pins
#define tenthX PB9
#define doubleX PB7
#define twoNhalfX PB8
#define coupPin PB12
//                                  0       1       2       3       4      5      6      7      8
const float DEFosciVmultTable[9] = {0.7353, 0.7353, 0.7353, 0.7353, 1.466, 3.663, 7.463, 14.91, 37.26};
float osciVmultTable[9] = {0.7353, 0.7353, 0.7353, 0.7353, 1.466, 3.663, 7.463, 14.91, 37.26};
byte osciVoltDiv = 5;
byte curosciVoltDiv = 255;
//vertical position
int8_t osciVpos = 0;
int8_t curosciVpos = 120;
//trigger level
#define trigOut PA2
HardwareTimer pwmtimer2(2);
int osciTlvl = 0;
int curtrigIndic = 1000;
//trigger mode
byte osciTmode = 0;  //2 single, 1 normal, 0 auto
byte curosciTmode = 255;
bool osciTslope = 1;  //1 positive, 2 negative
bool curosciTslope = 0;
//input coupling
bool osciCouple = 1; //1 DC, 0 AC
bool curosciCouple = 0;
//kalibracni obdelnik
#define compPin PB6
HardwareTimer pwmtimer4(4);
//extras
bool extras = 0;
float DCvolt = 0;
float RMSvolt = 0;
float Vmax = 0;
float Vmin = 0;
float freq = 0;
float duty = 0;
byte THDe = 0;
double THDn = 0;

//procesy pri spusteni
void setup() {
  Serial.begin(115200);
  delay(100);
  disableDebugPorts();
  delay(100);
  touchTFTInit();
  EEPROM.PageBase0 = 0x801F000;
  EEPROM.PageBase1 = 0x801F800;
  EEPROM.PageSize  = 0x400;
  loadCal();
  inputInit();
  delay(1000);
  tft.fillScreen(ILI9341_BLACK);
}

//hlavni cyklus
void loop() {
  checkBat();
  osciSample();
  TFTLoop();
  touchLoop();
}

//inicializace dotykového displaye
void touchTFTInit() {
  pinMode(Touch_CS, OUTPUT);
  digitalWrite(Touch_CS, HIGH);
  tft.begin(SPI, 55000000);
  Serial.println("TFT uspesne spusten");
  Serial.println("Dotykova obrazovka spustena");
  tft.setRotation(1);
  tft.fillScreen(ILI9341_BLACK);
  //logo
  tft.setCursor(40, 40);
  tft.setTextColor(ILI9341_YELLOW);
  tft.setTextSize(4);
  tft.print("STM32");
  tft.setCursor(40, 70);
  tft.print("EZ-Scope");
  tft.setTextSize(3);
  tft.setCursor(40, 100);
  tft.print(verze);
  tft.setCursor(40, 130);
  tft.print("Slehofer");
  tft.setCursor(40, 160);
  tft.print("PPC");
}

//inicializace pripojeni karty SD
void SDInit() {
  Serial.print("Zavadim SD kartu...");
  
  tft.setTextColor(ILI9341_YELLOW);
  tft.setTextSize(2);
  tft.setCursor(15, 55);
  
  if (!SD.begin(SD_CS)) {
    Serial.println("zavadeni selhalo!");
    tft.print("SD init fail");
    SDon = 0;
    return;
  }
  Serial.println("zavadeni uspesne");
  SDon = 1;
  const uint8_t BASE_NAME_SIZE = sizeof(FILE_BASE_NAME) - 1;
  char fileName[15] = FILE_BASE_NAME "00.csv";
  
  //nalezeni nepouzivaneho nazvu souboru
  while (SD.exists(fileName)) {
    if (fileName[BASE_NAME_SIZE + 1] != '9') {
      fileName[BASE_NAME_SIZE + 1]++;
    } 
    else if (fileName[BASE_NAME_SIZE] != '9') {
      fileName[BASE_NAME_SIZE + 1] = '0';
      fileName[BASE_NAME_SIZE]++;
    } 
    else {
      Serial.println("nelze vytvorit soubor");
      tft.print("File error 1");
    }
  }
  if (!file.open(fileName, O_CREAT | O_WRITE | O_EXCL)) {
    Serial.println("chyba otevreni souboru");
    tft.print("File error 2");
  }
  Serial.print(fileName);
  tft.print(fileName);
  Serial.println(" soubor vytvoren uspesne");
}

//inicializace vstupu
void inputInit() {
  //timer 2 PWM frequency adjust
  pwmtimer2.pause();
  pwmtimer2.setOverflow(4320);
  pwmtimer2.refresh();
  pwmtimer2.resume();
  //timer 4 PWM frequency adjust
  pwmtimer4.pause();
  pwmtimer4.setOverflow(36000);
  pwmtimer4.refresh();
  pwmtimer4.resume();
  //pin modes
  pinMode(trigOut, PWM);
  pinMode(analogInPin, INPUT_ANALOG);
  pinMode(batSens, INPUT);
  pinMode(TriggerPin, INPUT);
  pinMode(compPin, PWM);
  pinMode(coupPin, OUTPUT);
  pinMode(tenthX, OUTPUT);
  pinMode(doubleX, OUTPUT);
  pinMode(twoNhalfX, OUTPUT);
  adc_calibrate(ADC1);
  adc_calibrate(ADC2);
  pwmWrite(compPin, 18000);
}

//cyklus displaye
void TFTLoop() {
  if (millis() < (TFTLimit + 100)) return;
  TFTLimit = millis();

  //dynamicke prvky
  osciPrepare();
  int i = 0;
  int trigIndic = osciTlvl + osciVpos;
  if(trigIndic > 90) trigIndic = 90;
  if(trigIndic < -90) trigIndic = -90;
  
  switch (page) {
    //osciloskop
    case (0):
      //Vertical pos indicator
      if(osciVpos != curosciVpos){
        tft.fillRect(0, 20, 20, 200, ILI9341_BLACK);
        tft.drawLine(20, 121-osciVpos, 0, 111-osciVpos, ILI9341_YELLOW);
        tft.drawLine(19, 121-osciVpos, 0, 112-osciVpos, ILI9341_YELLOW);
        tft.drawLine(18, 121-osciVpos, 0, 113-osciVpos, ILI9341_YELLOW);
        tft.drawLine(20, 121-osciVpos, 0, 131-osciVpos, ILI9341_YELLOW);
        tft.drawLine(19, 121-osciVpos, 0, 130-osciVpos, ILI9341_YELLOW);
        tft.drawLine(18, 121-osciVpos, 0, 129-osciVpos, ILI9341_YELLOW);
        curosciVpos = osciVpos;
      }
      

      //Trig level indicator
      if(trigIndic != curtrigIndic){
        tft.fillRect(300, 20, 20, 200, ILI9341_BLACK);
        tft.drawLine(300, 121-trigIndic, 320, 111-trigIndic, ILI9341_WHITE);
        tft.drawLine(301, 121-trigIndic, 320, 112-trigIndic, ILI9341_WHITE);
        tft.drawLine(302, 121-trigIndic, 320, 113-trigIndic, ILI9341_WHITE);
        tft.drawLine(300, 121-trigIndic, 320, 131-trigIndic, ILI9341_WHITE);
        tft.drawLine(301, 121-trigIndic, 320, 130-trigIndic, ILI9341_WHITE);
        tft.drawLine(302, 121-trigIndic, 320, 129-trigIndic, ILI9341_WHITE);
        curtrigIndic = trigIndic;
      }

      //Battery indicator
      if(batLvl != curbatLvl){
        tft.fillRect(170, 3, 36, 25, ILI9341_BLACK);
        for(i = 0; i < batLvl; i++){
          tft.fillRect(170 + i*6, 3, 4, 25, ILI9341_WHITE);
        }
        curbatLvl = batLvl;
      }

      //Start stop button
      if(osciRun != curosciRun){
        tft.fillRect(60, 8, 60, 20, ILI9341_BLUE);
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(2);
        tft.setCursor(60, 8);
        if(osciRun){
          tft.print("STOP");
        }
        else tft.print("START");
        curosciRun = osciRun;
      }
      //Trig mode button
      if(osciTmode != curosciTmode){
        tft.fillRect(225, 8, 40, 20, ILI9341_BLUE);
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(2);
        tft.setCursor(225, 8);
        switch(osciTmode){
          case(0):
            tft.print("AUT");
            break;
          case(1):
            tft.print("NOR");
            break;
          case(2):
            tft.print("SGL");
            break;
        }
        curosciTmode = osciTmode;
      }

      //Vertical sensitivity
      if(osciVoltDiv != curosciVoltDiv){
        tft.fillRect(55, 218, 50, 20, ILI9341_BLACK);
        tft.setTextSize(2);
        tft.setCursor(55, 218);
        if(osciVoltDivTable[osciVoltDiv] > 50){
          if(osciVoltDivTable[osciVoltDiv] >= 1000) tft.print(osciVoltDivTable[osciVoltDiv] / 1000);
          else tft.print(((float)osciVoltDivTable[osciVoltDiv] / 1000.0), 1);
          tft.print("V");
        }
        else {
          tft.print(osciVoltDivTable[osciVoltDiv]);
          tft.print("mV");
        }
        curosciVoltDiv = osciVoltDiv;
      }
      //Timebase
      if(osciTimeDiv != curosciTimeDiv){
        tft.fillRect(186, 218, 58, 20, ILI9341_BLACK);
        tft.setTextSize(2);
        tft.setCursor(186, 218);
        if(osciTimeDivTable[osciTimeDiv] > 500){
          tft.print(osciTimeDivTable[osciTimeDiv] / 1000);
          tft.print("mS");
        }
        else {
          tft.print(osciTimeDivTable[osciTimeDiv]);
          tft.print("uS");
        }
        curosciTimeDiv = osciTimeDiv;
      }

      //Trig slope button
      if(osciTslope != curosciTslope){
        tft.fillRect(275, 218, 15, 20, ILI9341_BLUE);
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(2);
        tft.setCursor(275, 218);
        if(osciTslope) tft.print("P");
        else tft.print("N");
        curosciTslope = osciTslope;
      }

      //Coupling button
      if(osciCouple != curosciCouple){
        tft.fillRect(135, 218, 25, 20, ILI9341_BLUE);
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(2);
        tft.setCursor(135, 218);
        if(osciCouple) tft.print("DC");
        else tft.print("AC");
        curosciCouple = osciCouple;
      }
      if(newSamples) {
        osciSampleVal();
        drawGraticule();
        drawOsci();
        if(extras) drawExtras();
      }
      break;
      //settings
    case (1):
      //extras button
      if(extras)tft.fillRoundRect(295, 55, 20, 20, 5, ILI9341_WHITE);
      else tft.fillRoundRect(295, 55, 20, 20, 5, ILI9341_BLUE);
      
      //thd button
      if(THDe) tft.fillRoundRect(295, 95, 20, 20, 5, ILI9341_WHITE);
      else tft.fillRoundRect(295, 95, 20, 20, 5, ILI9341_BLUE);
      break;
      //calibration
    case (2):
      
      break;
      //export waveform
    case (3):
      
      break;
  }

  if (page == TFTpage) return;

  //staticke prvky
  tft.fillScreen(ILI9341_BLACK);
  switch (page) {
    //osciloskop
    case (0):
      //element redraw call
      curosciVpos = 120;
      curtrigIndic = 1000;
      curosciTimeDiv = 255;
      curosciVoltDiv = 255;
      curosciTslope = !osciTslope;
      curosciCouple = !osciCouple;
      curosciTmode = 255;
      curosciRun = !osciRun;
      curbatLvl = 255;
      
      //Vpos up button
      tft.fillRoundRect(0, 0, 20, 20, 5, ILI9341_BLUE);
      tft.drawLine(5, 15, 9, 5, ILI9341_WHITE);
      tft.drawLine(6, 15, 10, 5, ILI9341_WHITE);
      tft.drawLine(14, 16, 9, 5, ILI9341_WHITE);
      tft.drawLine(15, 16, 10, 5, ILI9341_WHITE);

      //Vpos down button
      tft.fillRoundRect(0, 220, 20, 20, 5, ILI9341_BLUE);
      tft.drawLine(5, 225, 9, 235, ILI9341_WHITE);
      tft.drawLine(6, 225, 10, 235, ILI9341_WHITE);
      tft.drawLine(14, 225, 9, 235, ILI9341_WHITE);
      tft.drawLine(15, 225, 10, 235, ILI9341_WHITE);

      //trig lvl up button
      tft.fillRoundRect(300, 0, 20, 20, 5, ILI9341_BLUE);
      tft.drawLine(305, 15, 309, 5, ILI9341_WHITE);
      tft.drawLine(306, 15, 310, 5, ILI9341_WHITE);
      tft.drawLine(314, 16, 309, 5, ILI9341_WHITE);
      tft.drawLine(315, 16, 310, 5, ILI9341_WHITE);

      //trig lvl down button
      tft.fillRoundRect(300, 220, 20, 20, 5, ILI9341_BLUE);
      tft.drawLine(305, 225, 309, 235, ILI9341_WHITE);
      tft.drawLine(306, 225, 310, 235, ILI9341_WHITE);
      tft.drawLine(314, 225, 309, 235, ILI9341_WHITE);
      tft.drawLine(315, 225, 310, 235, ILI9341_WHITE);

      //Hpos left button
      tft.fillRoundRect(27, 0, 20, 30, 5, ILI9341_BLUE);
      tft.drawLine(42, 4, 32, 14, ILI9341_WHITE);
      tft.drawLine(42, 5, 32, 15, ILI9341_WHITE);
      tft.drawLine(42, 24, 32, 14, ILI9341_WHITE);
      tft.drawLine(42, 25, 32, 15, ILI9341_WHITE);

      //Hpos right button
      tft.fillRoundRect(273, 0, 20, 30, 5, ILI9341_BLUE);
      tft.drawLine(278, 4, 288, 14, ILI9341_WHITE);
      tft.drawLine(278, 5, 288, 15, ILI9341_WHITE);
      tft.drawLine(278, 24, 288, 14, ILI9341_WHITE);
      tft.drawLine(278, 25, 288, 15, ILI9341_WHITE);

      //Start stop button
      tft.fillRoundRect(55, 0, 70, 30, 5, ILI9341_BLUE);
      
      //Settings button
      tft.fillRoundRect(135, 0, 20, 30, 5, ILI9341_BLUE);
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(2);
      tft.setCursor(140, 8);
      tft.print("S");

      //Battery indicator
      tft.drawRoundRect(165, 0, 45, 30, 5, ILI9341_WHITE);
      tft.fillRect(210, 10, 5, 10, ILI9341_WHITE);

      //Trig mode button
      tft.fillRoundRect(220, 0, 45, 30, 5, ILI9341_BLUE);

      //Vertical sensitivity - button
      tft.fillRoundRect(30, 210, 20, 30, 5, ILI9341_BLUE);
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(2);
      tft.setCursor(35, 218);
      tft.print("-");

      //Vertical sensitivity + button
      tft.fillRoundRect(105, 210, 20, 30, 5, ILI9341_BLUE);
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(2);
      tft.setCursor(110, 218);
      tft.print("+");

      //Coupling button
      tft.fillRoundRect(130, 210, 30, 30, 5, ILI9341_BLUE);

      //Timebase - button
      tft.fillRoundRect(165, 210, 20, 30, 5, ILI9341_BLUE);
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(2);
      tft.setCursor(170, 218);
      tft.print("-");

      //Timebase + button
      tft.fillRoundRect(245, 210, 20, 30, 5, ILI9341_BLUE);
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(2);
      tft.setCursor(250, 218);
      tft.print("+");

      //Trig slope button
      tft.fillRoundRect(270, 210, 20, 30, 5, ILI9341_BLUE);

      //Reticule frame
      tft.drawRect(21, 31, 279, 179, ILI9341_WHITE);
      drawGraticule();
      drawOsci();
      if(extras) drawExtras();
      break;
    //settings
    case (1):
      //return button
      tft.fillRoundRect(10, 10, 300, 30, 5, ILI9341_BLUE);
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(2);
      tft.setCursor(15, 15);
      tft.print("Return");
      
      //extras button
      tft.fillRoundRect(10, 50, 300, 30, 5, ILI9341_BLUE);
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(2);
      tft.setCursor(15, 55);
      tft.print("Extra info");
      
      //thd button
      tft.fillRoundRect(10, 90, 300, 30, 5, ILI9341_BLUE);
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(2);
      tft.setCursor(15, 95);
      tft.print("THD Analyze");
      
      //cal button
      tft.fillRoundRect(10, 130, 300, 30, 5, ILI9341_BLUE);
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(2);
      tft.setCursor(15, 135);
      tft.print("CALIBRATE");

      //export waveform button
      tft.fillRoundRect(10, 170, 300, 30, 5, ILI9341_BLUE);
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(2);
      tft.setCursor(15, 175);
      tft.print("Save Wave");
      break;
      //calibration
    case (2):
      //return button
      tft.fillRoundRect(10, 10, 300, 30, 5, ILI9341_BLUE);
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(2);
      tft.setCursor(15, 15);
      tft.print("Return");

      //null calibrate button
      tft.fillRoundRect(10, 50, 300, 30, 5, ILI9341_BLUE);
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(2);
      tft.setCursor(15, 55);
      tft.print("Offset Null");
      
      //10V calibrate button
      tft.fillRoundRect(10, 90, 300, 30, 5, ILI9341_BLUE);
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(2);
      tft.setCursor(15, 95);
      tft.print("10V Calibrate");
      
      //1V calibrate button
      tft.fillRoundRect(10, 130, 300, 30, 5, ILI9341_BLUE);
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(2);
      tft.setCursor(15, 135);
      tft.print("1V Calibrate");

      //save button
      tft.fillRoundRect(10, 170, 145, 30, 5, ILI9341_BLUE);
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(2);
      tft.setCursor(15, 175);
      tft.print("SAVE");

      //load button
      tft.fillRoundRect(165, 170, 145, 30, 5, ILI9341_BLUE);
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(2);
      tft.setCursor(170, 175);
      tft.print("LOAD");

      //restore button
      tft.fillRoundRect(10, 210, 300, 30, 5, ILI9341_BLUE);
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(2);
      tft.setCursor(15, 215);
      tft.print("Restore defaults");
      break;
      //export waveform
    case (3):
      //return button
      tft.fillRoundRect(10, 10, 300, 30, 5, ILI9341_BLUE);
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(2);
      tft.setCursor(15, 15);
      tft.print("Return");

      SDInit();
      if(SDon) saveWave();
      
      break;
  }
  TFTpage = page;
}

void osciSample(){
  if(!osciRun) return;
  if(newSamples) return;
  
  if(osciTimeDivTable[osciTimeDiv] > 250) tB = 1;
  else tB = 0;
  
  if(osciTimeDivTable[osciTimeDiv] > 5500){
    tB = 2;
    takeSlowSamples(osciTslope, osciTmode);
  }
  else takeSamples(tB, osciTslope, osciTmode);
  
  if(newSamples) sampleOffset();
}

//cyklus dotykové plochy
void touchLoop() {
  if (!ts.read_XY(xy)) {
    touchPressed = 0;
    touchInterval = 0;
    return;
  }
  if (touchPressed && (touchInterval < touchThr)){
    touchInterval++;
    return;
  }
  touchPressed = 1;
  
  if (millis() < (TouchLimit + 20) && touchPressed) return;
  TouchLimit = millis();
  
  xy[1] = map(xy[1], TS_MINY, TS_MAXY, 0, tft.height());
  xy[0] = map(xy[0], TS_MINX, TS_MAXX, 0, tft.width());
  
  xy[0] = tft.width() - xy[0];

  switch (page) {
    //osciloskop
    case (0):
      //tft.drawPixel(xy[0], xy[1], ILI9341_YELLOW); //debug

      //horni tlacitka
      if (xy[1] < 30) {
        //Vpos up button
        if (xy[0] < 20 && osciVpos < 90) {
          osciVpos++;
        }
        //Hpos left button
        if (xy[0] > 27 && xy[0] < 47 && osciHpos > 0) {
          osciHpos--;
        }
        //Start stop button
        if (xy[0] > 55 && xy[0] < 125) {
          osciRun = !osciRun;
        }
        //Settings button
        if (xy[0] > 135 && xy[0] < 155) {
          page = 1;
        }
        //Trig mode button
        if (xy[0] > 220 && xy[0] < 265) {
          osciTmode++;
          if(osciTmode > 2) osciTmode = 0;
        }
        //Hpos right button
        if (xy[0] > 273 && xy[0] < 293 && osciHpos < 280) {
          osciHpos++;
        }
        //trig lvl up button
        if (xy[0] > 300 && osciTlvl < 180) {
          osciTlvl++;
        }
      }
      //dolni tlacitka
      if (xy[1] > 210) {
        //Vpos down button
        if (xy[0] < 20 && osciVpos > -90) {
          osciVpos--;
        }
        //Vertical sensitivity - button
        if (xy[0] > 30 && xy[0] < 50 && osciVoltDiv > 0) {
          osciVoltDiv--;
        }
        //Vertical sensitivity + button
        if (xy[0] > 105 && xy[0] < 125 && osciVoltDiv < 8) {
          osciVoltDiv++;
        }
        //Coupling button
        if (xy[0] > 130 && xy[0] < 160) {
          osciCouple = !osciCouple;
        }
        //Timebase - button
        if (xy[0] > 165 && xy[0] < 185 && osciTimeDiv > 0) {
          osciTimeDiv--;
        }
        //Timebase + button
        if (xy[0] > 245 && xy[0] < 265 && osciTimeDiv < 14) {
          osciTimeDiv++;
        }
        //Trig slope button
        if (xy[0] > 270 && xy[0] < 290) {
          osciTslope = !osciTslope;
        }
        //trig lvl down button
        if (xy[0] > 300 && osciTlvl > -180) {
          osciTlvl--;
        }
      }
      break;
      //settings
    case (1):
      //return button
      if(xy[1] < 40) page = 0;

      //extras button
      if(xy[1] > 50 && xy[1] < 80) {
        extras = !extras;
      }
      
      //thd button
      if(xy[1] > 90 && xy[1] < 120) {
        THDe = !THDe;
      }

      //cal button
      if(xy[1] > 130 && xy[1] < 160) {
        page = 2;
      }

      //export waveform button
      if(xy[1] > 170 && xy[1] < 200) {
        page = 3;
      }
      break;
      //calibration
    case (2):
      //return button
      if(xy[1] < 40) page = 1;
      
      //null calibrate
      if(xy[1] > 50 && xy[1] < 80) {
        offNull();
      }
      
      //10v calibrate
      if(xy[1] > 90 && xy[1] < 120) {
        tenVoltCal();
      }

      //1v calibrate
      if(xy[1] > 130 && xy[1] < 160) {
        voltCal();
      }

      //save load
      if(xy[1] > 170 && xy[1] < 200) {
        //save
        if(xy[0] < 160) {
          saveCal();
        }
        //load
        else {
          loadCal();
        }
      }
      
      //restore defaults
      if(xy[1] > 210) {
        restoreCal();
      }
      break;
      //save waveform
    case (3):
      //return button
      if(xy[1] < 40) page = 1;
      
      break;
  }
}

//ulozeni prubehu
void saveWave(){
  file.println("t[uS];U[mV]");
  file.println();

  float timeSamp = sRateTable[tB] / 14.0;

  if(osciTimeDivTable[osciTimeDiv] < 10.0){
    timeSamp /= 10.0;
  }
  
  for(uint16_t i = 0; i < maxSamples; i++){
    float val = dataPoints[i];
    val *= osciVmultTable[osciVoltDiv];
    fileprintc(i * timeSamp, 3);
    file.print(";");
    fileprintc(val, 1);
    file.println();
  }
  //log extra info
  if(extras){
    file.println();
    //DC volt
    file.print("Average:;");
    if(DCvolt > 500 || DCvolt < -500){
      if(DCvolt > 5000 || DCvolt < -5000)  fileprintc(DCvolt / 1000.0, 1);
      else  fileprintc(DCvolt / 1000.0, 2);
      file.println("V");
    }
    else {
      fileprintc(DCvolt, 1);
      file.println("mV");
    }
    //RMS volt
    file.print("RMS Average:;");
    if(RMSvolt > 500){
      if(RMSvolt > 5000)  fileprintc(RMSvolt / 1000.0, 1);
      else  fileprintc(RMSvolt / 1000.0, 2);
      file.println("V");
    }
    else {
      fileprintc(RMSvolt, 1);
      file.println("mV");
    }

    //Max volt
    file.print("Maximum:;");
    if(Vmax > 500 || Vmax < -500){
      if(Vmax > 5000 || Vmax < -5000)  fileprintc(Vmax / 1000.0, 1);
      else  fileprintc(Vmax / 1000.0, 2);
      file.println("V");
    }
    else {
      fileprintc(Vmax, 1);
      file.println("mV");
    }
    //Min volt
    file.print("Minimum:;");
    if(Vmin > 500 || Vmin < -500){
      if(Vmin > 5000 || Vmin < -5000)  fileprintc(Vmin / 1000.0, 1);
      else  fileprintc(Vmin / 1000.0, 2);
      file.println("V");
    }
    else {
      fileprintc(Vmin, 1);
      file.println("mV");
    }

    //frequency
    file.print("Frequency:;");
    if(freq > 500){
      if(freq > 5000)  fileprintc(freq / 1000.0, 2);
      else  fileprintc(freq / 1000.0, 3);
      file.println("kHz");
    }
    else {
      fileprintc(freq, 1);
      file.println("Hz");
    }
    //dutycycle
    file.print("Duty:;");
    fileprintc(duty * 100, 1);
    file.println("%");
  
    //harmonicke zkresleni
    if(THDe){
      file.print("THD+N:;");
      if(THDn > 0.01)  fileprintc(THDn * 100, 1);
      else  fileprintc(THDn * 100, 2);
      file.println("%");
    }
  }
  
  tft.setCursor(15, 95);
  
  //nahraj data do karty SD
  if (!file.sync() || file.getWriteError()) {
    Serial.println("write error");
    tft.print("Write error");
  }
  else{
    tft.print("Wave saved");
  }
  file.close();
  SDon = 0;
}

//kalibrace nuly
void offNull(){
  digitalWrite(coupPin, 1);
  digitalWrite(tenthX, 0);
  digitalWrite(doubleX, 1);
  digitalWrite(twoNhalfX, 1);
  delay(50);
  takeSamples(0, 1, 0);
  
  uint32_t val = 0;
  for(uint16_t i = 100; i < maxSamples - 100; i++){
    val += dataPoints[i];
  }
  val /= maxSamples - 200;

  nullpoint[0] = val;

  takeSamples(1, 1, 0);
  
  val = 0;
  for(uint16_t i = 100; i < maxSamples - 100; i++){
    val += dataPoints[i];
  }
  val /= maxSamples - 200;

  nullpoint2[0] = val;

  digitalWrite(tenthX, 0);
  digitalWrite(doubleX, 0);
  digitalWrite(twoNhalfX, 0);

  takeSamples(0, 1, 0);
  
  val = 0;
  for(uint16_t i = 100; i < maxSamples - 100; i++){
    val += dataPoints[i];
  }
  val /= maxSamples - 200;

  nullpoint[1] = val;

  takeSamples(1, 1, 0);
  
  val = 0;
  for(uint16_t i = 100; i < maxSamples - 100; i++){
    val += dataPoints[i];
  }
  val /= maxSamples - 200;

  nullpoint2[1] = val;
  
  TFTpage = 255;
}

//kalibrace citlivosti na 1V
void voltCal(){
  tB = 0;
  osciCouple = 1;
  
  osciVoltDiv = 0;
  osciPrepare();
  delay(50);
  
  takeSamples(0, 1, 0);
  sampleOffset();
  
  float val = 0;
  for(uint16_t i = 100; i < maxSamples - 100; i++){
    val += dataPoints[i];
  }
  val /= maxSamples - 200;
  
  val = 1000.0 / val;

  osciVmultTable[0] = val;
  osciVmultTable[1] = val;
  osciVmultTable[2] = val;
  osciVmultTable[3] = val;

  osciVoltDiv = 4;
  osciPrepare();
  delay(50);
  
  takeSamples(0, 1, 0);
  sampleOffset();
  
  val = 0;
  for(uint16_t i = 100; i < maxSamples - 100; i++){
    val += dataPoints[i];
  }
  val /= maxSamples - 200;
  
  val = 1000.0 / val;
  
  osciVmultTable[4] = val;

  osciVoltDiv = 5;
  osciPrepare();
  delay(50);
  
  takeSamples(0, 1, 0);
  sampleOffset();
  
  val = 0;
  for(uint16_t i = 100; i < maxSamples - 100; i++){
    val += dataPoints[i];
  }
  val /= maxSamples - 200;
  
  val = 1000.0 / val;
  
  osciVmultTable[5] = val;
  
  TFTpage = 255;
}

//kalibrace citlivosti na 10V
void tenVoltCal(){
  tB = 0;
  osciCouple = 1;
  
  osciVoltDiv = 6;
  osciPrepare();
  delay(50);
  
  takeSamples(0, 1, 0);
  sampleOffset();
  
  float val = 0;
  for(uint16_t i = 100; i < maxSamples - 100; i++){
    val += dataPoints[i];
  }
  val /= maxSamples - 200;
  
  val = 10000.0 / val;

  osciVmultTable[6] = val;

  osciVoltDiv = 7;
  osciPrepare();
  delay(50);
  
  takeSamples(0, 1, 0);
  sampleOffset();
  
  val = 0;
  for(uint16_t i = 100; i < maxSamples - 100; i++){
    val += dataPoints[i];
  }
  val /= maxSamples - 200;
  
  val = 10000.0 / val;
  
  osciVmultTable[7] = val;

  osciVoltDiv = 8;
  osciPrepare();
  delay(50);
  
  takeSamples(0, 1, 0);
  sampleOffset();
  
  val = 0;
  for(uint16_t i = 100; i < maxSamples - 100; i++){
    val += dataPoints[i];
  }
  val /= maxSamples - 200;
  
  val = 10000.0 / val;
  
  osciVmultTable[8] = val;
  
  TFTpage = 255;
}

//ulozeni kalibracnich dat do pameti eeprom
void saveCal(){
  int i = 0;
  EEPROM.put(i, osciVmultTable);
  i += 9 * ((sizeof(float) + 1) / 2);
  EEPROM.put(i, nullpoint);
  i += 2;
  EEPROM.put(i, nullpoint2);
  TFTpage = 255;
}

//nacteni kalibracnich dat z pameti eeprom
void loadCal(){
  int i = 0;
  EEPROM.get(i, osciVmultTable);
  i += 9 * ((sizeof(float) + 1) / 2);
  EEPROM.get(i, nullpoint);
  i += 2;
  EEPROM.get(i, nullpoint2);
  TFTpage = 255;
}

//obnoveni vychozich hodnot kalibrace
void restoreCal(){
  for(int i = 0; i < 9; i++){
    osciVmultTable[i] = DEFosciVmultTable[i];
  }
  nullpoint[0] = DEFnullpoint;
  nullpoint2[0] = DEFnullpoint2;
  nullpoint[1] = DEFnullpointb;
  nullpoint2[1] = DEFnullpoint2b;
  TFTpage = 255;
}

//vykresleni dodatecnych informaci
void drawExtras(){
  //tft.fillRect(22, 32, 277, 36, ILI9341_BLACK);
  tft.setTextColor(ILI9341_WHITE);
  tft.setTextSize(2);
  tft.setCursor(25, 35);
  //DC volt
  if(DCvolt > 500 || DCvolt < -500){
    if(DCvolt > 5000 || DCvolt < -5000) tft.print(DCvolt / 1000.0, 1);
    else tft.print(DCvolt / 1000.0, 2);
    tft.print("Vavg ");
  }
  else {
    tft.print(DCvolt, 1);
    tft.print("mVavg ");
  }
  //RMS volt
  if(RMSvolt > 500){
    if(RMSvolt > 5000) tft.print(RMSvolt / 1000.0, 1);
    else tft.print(RMSvolt / 1000.0, 2);
    tft.print("Vrms ");
  }
  else {
    tft.print(RMSvolt, 1);
    tft.print("mVrms");
  }
  tft.setCursor(25, 53);
  //Max volt
  if(Vmax > 500 || Vmax < -500){
    if(Vmax > 5000 || Vmax < -5000) tft.print(Vmax / 1000.0, 1);
    else tft.print(Vmax / 1000.0, 2);
    tft.print("Vmax ");
  }
  else {
    tft.print(Vmax, 1);
    tft.print("mVmax ");
  }
  //Min volt
  if(Vmin > 500 || Vmin < -500){
    if(Vmin > 5000 || Vmin < -5000) tft.print(Vmin / 1000.0, 1);
    else tft.print(Vmin / 1000.0, 2);
    tft.print("Vmin ");
  }
  else {
    tft.print(Vmin, 1);
    tft.print("mVmin");
  }

  //tft.fillRect(22, 191, 277, 18, ILI9341_BLACK);
  tft.setCursor(25, 194);

  //frequency
  if(freq > 500){
    if(freq > 5000) tft.print(freq / 1000.0, 2);
    else tft.print(freq / 1000.0, 3);
    tft.print("kHz ");
  }
  else {
    tft.print(freq, 1);
    tft.print("Hz ");
  }
  //dutycycle
  tft.print(duty * 100, 1);
  tft.print("%");
  
  //harmonicke zkresleni
  if(THDe){
    //tft.fillRect(22, 173, 277, 18, ILI9341_BLACK);
    tft.setCursor(25, 176);
    tft.print("THD+N:");
    if(THDn > 0.01) tft.print(THDn * 100, 1);
    else tft.print(THDn * 100, 2);
    tft.print("%");
  }
}

//priprava na vzorkovani
void osciPrepare(){
  //trigger level
  int trigLvl = osciVoltDivTable[osciVoltDiv];
  if(trigLvl > 100) trigLvl = 100;
  trigLvl = 2160 - osciTlvl*((float)trigLvl*0.08);
  pwmWrite(trigOut,trigLvl);
  //input sensitivity select
  byte sel = osciVSelectTable[osciVoltDiv];
  digitalWrite(tenthX, bitRead(sel, 0));
  digitalWrite(doubleX, bitRead(sel, 1));
  digitalWrite(twoNhalfX, bitRead(sel, 2));
  //input coupling select
  digitalWrite(coupPin, osciCouple);
}

//mereni napeti baterie
void checkBat(){
  if(tB == 2 && osciRun) return;
  if (millis() < (BatLimit + 200)) return;
  BatLimit = millis();

  analogRead(batSens);
  analogRead(batSens);
  int val = analogRead(batSens);
  //int val = 3600;
  //Serial.println(val);
  val -= 3050;
  if(val < 0) val = 0;
  batLvl = val / 95;
  if(batLvl > 6) batLvl = 6;
}

//interpolace dat
void sampleFIRSINCinterpol(){
  //pole vstupnich vzorku
  int16_t origSamples[maxSamples / 10];
  for(int i = 0; i < (maxSamples / 10); i++){
    origSamples[i] = dataPoints[i];
  }

  //FIR sinc filtr
  for(int i = 0; i < maxSamples; i++){
    float accu = 0;
    for(int c = 0; c < 31; c++){
      int index = 15 - c + i;
      
      if(index % 10 == 0 && index >= 0){
        byte coefIx;
        if(c > 15) coefIx = 30 - c;
        else coefIx = c;

        accu += FIRcoef[coefIx] * origSamples[index / 10];;
      }
    }
    dataPoints[i] = accu;
  }
}

//aplikace offsetu adc
void sampleOffset(){
  int nul = 0;
  int ix = 0;

  if(osciVoltDiv == 5 || osciVoltDiv == 8) ix = 1;
  
  if(tB) nul = nullpoint2[ix];
  else nul = nullpoint[ix];
  
  for(uint16_t i = 0; i < maxSamples; i++){
      dataPoints[i] = -dataPoints[i] + nul;
  }
}

//analyza a skalovani hodnot osciloskopu
void osciSampleVal() {
  //casova zakladna
  float timemult = osciTimeDivTable[osciTimeDiv];
  float timeSamp = sRateTable[tB] / 14.0;
  
  if(timemult < 10.0){
    timemult *= 10.0;
    timeSamp /= 10.0;
    sampleFIRSINCinterpol();
  }
  
  timemult /= sRateTable[tB];
  
  //vertikalni citlivost
  float voltmult = 22.25 * osciVmultTable[osciVoltDiv] / (float)osciVoltDivTable[osciVoltDiv];
  
  if(extras){
    DCvolt = 0.0;
    RMSvolt = 0.0;
    Vmax = -2100.0;
    Vmin = 2100.0;
    freq = 0.0;
    duty = 0.0;
    //analyza napeti
    for(uint16_t i = 10; i < maxSamples - 10; i++){
      float val = dataPoints[i];
      DCvolt += val;
      RMSvolt += val * val;
      //error smoothing
      val += dataPoints[i - 2] + dataPoints[i - 1] + dataPoints[i + 1] + dataPoints[i + 2];
      val /= 5;
      if(val > Vmax) Vmax = val;
      if(val < Vmin) Vmin = val;
    }
    DCvolt /= maxSamples - 20;
    
    //analyza casu
    byte stage = 0;
    float strt = 1;
    float mid = 2;
    float stp = 3;
    int off = ((int)Vmax + (int)Vmin) / 2;
    for(uint16_t i = 10; i < maxSamples - 10; i++){
      int a = 0;
      int b = 0;
      for(int c = 0; c < 8; c++){
        a += dataPoints[i - c];
        b += dataPoints[i + c];
      }
      a -= off;
      b -= off;
      
      if(b > 0 && a < 0){
        if (a < 0) a = -a;
        if (b < 0) b = -b;
        float shift = (float)a / (float)(a + b);
        if(stage == 2){
          stp = (float)i + shift;
          i = maxSamples;
        }
        if(stage == 0) {
          strt = (float)i + shift;
          stage ++;
        }
      }
      if(b < 0 && a > 0 && stage == 1){
        if (a < 0) a = -a;
        if (b < 0) b = -b;
        float shift = (float)a / (float)(a + b);
        mid = (float)i + shift;
        stage++;
      }
    }
    //analyza harmonickeho zkresleni
    if(THDe){
      THDNlyze(20, strt, stp);
    }
    //vypocet stridy a frekvence
    duty = (mid - strt) / (stp - strt);
    if(duty > 1.0) duty = 1.0;
    if(duty < 0.0) duty = 0.0;
    stp *= timeSamp;
    strt *= timeSamp;
    freq = 1000000.0 / (stp - strt);
    if(freq < 0.0) freq = 0.0;
    if(freq > 700000.0) freq = 0.0;
    //vypocet hodnot napeti
    Vmax *= osciVmultTable[osciVoltDiv];
    Vmin *= osciVmultTable[osciVoltDiv];
    DCvolt *= osciVmultTable[osciVoltDiv];
    RMSvolt /= maxSamples - 20;
    RMSvolt = sqrt(RMSvolt);
    RMSvolt *= osciVmultTable[osciVoltDiv];
  } 

  //data pro kresleni prubehu
  for (uint16_t i = 0; i < 139; i++) {
    float pos = (i + osciHpos) * timemult;
    uint16_t index = (int)pos;
    float interpolate = pos - (float)index;
    if(index + 1 > maxSamples) index = maxSamples - 1;
    
    float val = 0;
    val = (1.0 - interpolate) * (float)(dataPoints[index]) + interpolate * (float)dataPoints[index + 1];
    //val = dataPoints[i]; //debug
    val *= voltmult; //prepocet na napeti
    //zaokrouhleni
    if(val > 0.0) val += 0.5;
    else val -= 0.5;
    val += osciVpos;  //aplikace vertikalniho posuvu
    val += 89.0;
    if(val > 179) val = 179; //limit maxima
    if(val < 1) val = 1;  //limit minima
    osciDisplay[i] = val;
  }
  newSamples = 0;
}

//vykresleni stinitka osciloskopu
void drawGraticule() {
  tft.fillRect(RetStartX + 1, RetStartY + 1, RetWidth - 3, RetHeight - 3, ILI9341_BLACK);
  
  for (uint16_t TicksX = 1; TicksX < RetXDiv; TicksX++)
  {
    for (uint16_t TicksY = 1; TicksY < RetYDiv; TicksY++)
    {
      tft.drawPixel(RetStartX + (TicksX * RetWidth) / RetXDiv, RetStartY + (TicksY * RetHeight) / RetYDiv, RetColor);
    }
  }
  for (uint16_t TicksX = 1; TicksX < RetXDiv; TicksX++)
  {
    tft.drawFastVLine(RetStartX + (TicksX * RetWidth) / RetXDiv, RetStartY + (RetHeight / 2) - 3, 7, RetColor);
  }
  for (uint16_t TicksX = 1; TicksX < (RetXDiv*5); TicksX++)
  {
    tft.drawFastVLine(RetStartX + (TicksX * RetWidth) / (RetXDiv*5), RetStartY + (RetHeight / 2) - 1, 3, RetColor);
  }
  /*
  for (uint16_t TicksX = 0; TicksX < 50; TicksX++)
  {
    uint16_t xpos = (TicksX * RetWidth) / 50;
    if (xpos % (RetWidth / 10) > 0 )
    {
      tft.drawFastVLine(RetStartX + xpos, RetStartY + (RetHeight / 2) - 1, 3, RetColor);
    }
    else
    {
      tft.drawFastVLine(RetStartX + xpos, RetStartY + (RetHeight / 2) - 3, 7, RetColor);
    }

  }
  */
  for (uint16_t TicksY = 1; TicksY < RetYDiv; TicksY++)
  {
    tft.drawFastHLine(RetStartX + (RetWidth / 2) - 3, RetStartY + (TicksY * RetHeight) / RetYDiv, 7, RetColor);
  }
  for (uint16_t TicksY = 1; TicksY < (RetYDiv*5); TicksY++)
  {
    tft.drawFastHLine(RetStartX + (RetWidth / 2) - 1, RetStartY + (TicksY * RetHeight) / (RetYDiv*5), 3, RetColor);
  }
}

//vykres prubehu napeti na osciloskop
void drawOsci() {
  for (int i = 0; i < ((RetWidth - 2) / 2) - 1; i++) {
    tft.drawLine(RetStartX + 2 * i + 1, (RetStartY + RetHeight - 1) - osciDisplay[i],RetStartX + 2 * i + 3, (RetStartY + RetHeight - 1) - osciDisplay[i + 1], beamColor);
  }
  tft.drawRect(21, 31, 279, 179, ILI9341_WHITE);
}

//funkce pro zapis dessetinych cisel na kartu SD s carkou jako oddelovacem dessetin
void fileprintc(float val, uint32_t dec){
  char buffer[] = "99999.999";
  
  dtostrf(val, 0, dec, buffer);
  char* p  = strchr(buffer, '.');
  *p = ',';
  file.print(buffer);
}

//vypocet harmonickeho zkresleni
void THDNlyze(byte HarmonicsCount, float strt, float stp) {
  uint16_t outputSampleCount = 2 * HarmonicsCount + 2;
  double sampledData[42];

  //delka vybraneho prubehu
  double delka = stp - strt;
  uint16_t zac = strt;

  //supersampling
  double sampleStep = delka / float(outputSampleCount);
  uint16_t sampleSize = sampleStep / 2;
  for (uint16_t i = 0; i < outputSampleCount; i++) {
    //zvol dve okoli a pocitej dva prumery
    int32_t smpl1 = 0;
    int32_t smpl2 = 0;
    float step = i * sampleStep;
    for (uint16_t c = zac + uint16_t(step) - sampleSize; c <= zac + uint16_t(step) + sampleSize; c++) {
      smpl1 += dataPoints[c];
      smpl2 += dataPoints[c + 1];
    }
    double sample1 = smpl1;
    double sample2 = smpl2;
    sample1 /= 2 * sampleSize + 1;
    sample2 /= 2 * sampleSize + 1;
    //skalovana interpolace
    double dif = step - double(uint16_t(step));
    sampledData[i] = sample1 * (1.0 - dif) + sample2 * dif;
  }

  //odstraneni DC slozky
  double dcoff = 0;
  for (uint16_t i = 0; i < outputSampleCount; i++) {
    dcoff += sampledData[i];
  }
  dcoff /= (float)outputSampleCount;
  for (uint16_t i = 0; i < outputSampleCount; i++) {
    sampledData[i] -= dcoff;
  }
  
  //harmonicka analyza
  double a = 0;
  double b = 0;
  double rad = 6.28318531 / (double)outputSampleCount;
  for (uint16_t i = 0; i < outputSampleCount; i++) {
    a += sampledData[i] * cos(rad * (float)i);
    b += sampledData[i] * sin(rad * (float)i);
  }
  a /= (outputSampleCount / 2);
  b /= (outputSampleCount / 2);

  //analyza harmonickeho zkresleni se sumem
  double noiseRMS = 0;
  for (uint16_t i = 0; i < outputSampleCount; i++) {
    //synteza idealniho sinu
    double noiseData = a * cos(rad * (double)i) + b * sin(rad * (double)i);
    //rms akumulace rozdilu od idealu
    noiseRMS += sq(noiseData - sampledData[i]);
  }
  noiseRMS /= (double)outputSampleCount;
  noiseRMS = sqrt(noiseRMS);

  THDn = 1.41421356 * noiseRMS / sqrt(sq(a) + sq(b));
  if(THDn > 1.0) THDn = 1.0;
}

//funkce pro pomaly sber vzorku
void takeSlowSamples(bool triggerSlope, byte triggerMode){
  if(!slowSampling){
    if(digitalRead(TriggerPin) == !triggerSlope && timeout < 10000 && !slowTstage) {
      timeout++;
    }
    else if(!slowTstage){
      timeout = 0;
      slowTstage = 1;
    }
    if(digitalRead(TriggerPin) == triggerSlope && slowTstage) {
      if (timeout > 10000){
        slowSampling = 1;
        timeout = 0;
      }
      else timeout++;
    }
    else if(slowTstage){
      timeout = 0;
      trigged = 1;
      slowSampling = 1;
    }
  }
  if(slowSampling){
    //vzorkovani
    if((trigged || triggerMode == 0) && sampleIter < maxSamples){
      dataPoints[sampleIter] = analogRead(analogInPin);
      sampleIter++;
    }
    else {
      if(trigged || triggerMode == 0) newSamples = 1;
      //doslo k jednorazovemu spusteni
      if(trigged && triggerMode == 2) osciRun = 0;
      trigged = 0;
      slowSampling = 0;
      sampleIter = 0;
      slowTstage = 0;
    }
  }
}

//funkce pro sber vzorku
//tato funkce pouziva dvoji stridave vzorkovani pro maximalizaci rychlosti ADC
void takeSamples(bool delayStep, bool triggerSlope, byte triggerMode) {
  //             rychlost vzorkovani| 1 nabezna hrana, 0 klesajici hrana| 2 single, 1 normal, 0 auto
  
  analogRead(analogInPin);
  
  //rychlost vzorkovani
  //pomala
  if(delayStep) {
      adc_set_sample_rate(ADC1, ADC_SMPR_239_5); //10.49us na vzorek
      adc_set_sample_rate(ADC2, ADC_SMPR_239_5);
  }
  //rychla
  else {
      adc_set_sample_rate(ADC1, ADC_SMPR_1_5); //0,58uS na vzorek
      adc_set_sample_rate(ADC2, ADC_SMPR_1_5);
  }
  int pinMapADCin = PIN_MAP[analogInPin].adc_channel;
  
  adc_set_reg_seqlen(ADC1, 1);
  adc_set_reg_seqlen(ADC2, 1);
  
  ADC1->regs->SQR3 = pinMapADCin;
  
  ADC1->regs->CR1 |= ADC_CR1_FASTINT;

  ADC1->regs->CR2 |= ADC_CR2_CONT | ADC_CR2_SWSTART;
  ADC2->regs->CR2 |= ADC_CR2_CONT | ADC_CR2_SWSTART;

  ADC2->regs->CR1 |= ADC_CR1_FASTINT;
  
  ADC2->regs->SQR3 = pinMapADCin;
  
  timeout = 0;
  
  //spousteni
  trigged = 1;
  //while (digitalRead(TriggerPin) == !triggerSlope) {
  while (digitalRead(TriggerPin) == !triggerSlope) {
    if (timeout > 65530){
      trigged = 0;
      break;
    }
    timeout++;
  }
  timeout = 0;
  if(trigged){
    while (digitalRead(TriggerPin) == triggerSlope) {
      if (timeout > 65530){
        trigged = 0;
        break;
      }
      timeout++;
    }
  }
  
  //spusteni vzorkovani
  if(trigged || triggerMode == 0){
    
    nvic_globalirq_disable();
    for (uint16_t j = 0; j < maxSamples; j += 2) {
      while (!(ADC1->regs->SR & ADC_SR_EOC));
      dataPoints[j] = ADC1->regs->DR & ADC_DR_DATA;
      while (!(ADC2->regs->SR & ADC_SR_EOC));
      dataPoints[j+1] = ADC2->regs->DR & ADC_DR_DATA;
    }
    nvic_globalirq_enable();

    //informace, ze byly nabrany nove vzorky
    newSamples = 1;
  }
  
  //doslo k jednorazovemu spusteni
  if(trigged && triggerMode == 2) osciRun = 0;
  trigged = 0;
  timeout = 0;
}
