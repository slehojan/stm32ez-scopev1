# STM32F103 osciloskop - STM32EZ-ScopeV1

Práce spočívá v návrhu a sestavení jednokanálového osciloskopu za využití procesoru STM32F103CB z modulu Blue Pill.
Výsledný osciloskop disponuje uživatelským rozhraním ve formě dotykové obrazovky s interaktivním grafickým rozhraním. Dále má nastavitelnou časovou základnu o 15 rychlostech (1 us/div až 50 ms/div), 9 vertikálních citlivostí (10 mV/div až 5 V/div, řízené procesorem) a nastavitelnou úroveň a režim spouštění. Disponuje pracovním pásmem 300 kHz a vzorkovací rychlostí 1,7 MSPS (17 MSPS s interpolací). Vstup osciloskopické normy impedance 1 Mohm a kapacity 15pF s ochranou. Veškerá elektronika je umístěna na vlastním plošném spoji s integrovanou baterií o kapacitě 200 mAh, nabíjení je realizováno pomocí mini USB portu.

Osciloskop umožňuje změřený průběh exportovat do připojené SD karty ve formátu CSV a počítá základní parametry měřeného průběhu, tedy maximální a minimální hodnotu, střední (průměrnou) a efektivní hodnotu, frekvenci a střídu. Dále má volitelnou funkci počítání harmonického zkreslení se šumem u měřeného signálu, což je užitečné např. pro měření zkreslení zvukových zesilovačů či seřizování sinusových tvarovačů analogových generátorů funkcí.

Pro velmi rychlé průběhy (časová zakladna rychlejší než 10 us/div) využívá SINC (sin x / x) interpolaci, címž umožňuje pohodlné vyčtení hodnot i signálů na hranici pracovního pásma. Interpolace je implementována pomocí 31 prvkového digitálního FIR LP filtru.

Pro maximalizaci přesnosti měření je v uživatelském rozhraní implementováno kalibrační menu, ve kterém lze kalibrovat nulu vstupního signálu, všechny vertikální citlivosti a kalibrační data následně uložit či načíst z vnitřní paměti nebo obnovit výchozí hodnoty kalibrace.

## Schéma zapojení:
![](/Docs/STM32EZ-ScopeV1-Schematic.png)

## Návrh plošného spoje:
![](/Docs/STM32EZ-ScopeV1-Layout.jpg)

## Hotové plošné spoje z výroby:
![](/Docs/STM32EZ-ScopeV1-PCB.jpg)

## Osazená elektronika:
![](/Docs/STM32EZ-ScopeV1-Photo1.jpg)
![](/Docs/STM32EZ-ScopeV1-Photo2.jpg)
![](/Docs/STM32EZ-ScopeV1-Photo3.jpg)

## Hotový osciloskop:
![](/Docs/STM32EZ-ScopeV1-Photo4.jpg)
![](/Docs/STM32EZ-ScopeV1-Photo5.jpg)
![](/Docs/STM32EZ-ScopeV1-Photo6.jpg)
![](/Docs/STM32EZ-ScopeV1-Probe.jpg)

## Uživatelské rozhraní osciloskopu:
![](/Docs/STM32EZ-ScopeV1-BootScreen.jpg)
![](/Docs/STM32EZ-ScopeV1-Interface.jpg)
![](/Docs/STM32EZ-ScopeV1-Extras.jpg)
![](/Docs/STM32EZ-ScopeV1-Settings.jpg)
![](/Docs/STM32EZ-ScopeV1-CalibrationMenu.jpg)

## Ukázka měřených průběhů:
![](/Docs/STM32EZ-ScopeV1-Sine.jpg)
![](/Docs/STM32EZ-ScopeV1-Square.jpg)
![](/Docs/STM32EZ-ScopeV1-Triangle.jpg)
